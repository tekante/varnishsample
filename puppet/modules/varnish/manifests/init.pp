# Most puppet work is done in classes, which are almost always contained in
# modules.
#
# Below is a sample custom varnish class, wrapped in a varnish module
#
# This file, and the directory that contains it, conform to the standard
# layout common among puppet modules, although I've chosen not to include
# some of the common dependency and documentation files for simplicity
#
# I'm also including EVERYTHING necessary to deploy Varnish in this
# single class.  Most puppet modules would split these up in order
# to make reuse simpler

class varnish {

  # The most common pattern for deployments is
  # 1. configure Yum to provide packages
  # 2. install those packages
  # 3. deploy the any configuration necessary
  # 4. configure and launch the target service.
  # which we'll follow below

  # Configure the Varnish GPG Key
  file { '/etc/pki/rpm-gpg/RPM-GPG-KEY-VARNISH':
      ensure   => 'file',
      source   => 'puppet:///modules/varnish/GPGKEY',
      group    => '0',
      mode     => '0644',
      owner    => '0',
  }

  # Deploy the actual Varnish Repo
  yumrepo { 'varnish-4.0':
    ensure   => 'present',
    baseurl  => 'https://repo.varnish-cache.org/redhat/varnish-4.0/el6/$basearch',
    descr    => 'Varnish 4.0 for Enterprise Linux',
    enabled  => '1',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-VARNISH',
    require  => File['/etc/pki/rpm-gpg/RPM-GPG-KEY-VARNISH'],
  }

  # Deploy EPEL GPG Key
  file { '/etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6':
    ensure   => 'file',
    source   => 'puppet:///modules/varnish/EPELGPG',
    group    => '0',
    mode     => '0644',
    owner    => '0',
  }

  # Deploy EPEL Repo
  yumrepo { 'epel':
    ensure         => 'present',
    descr          => 'Extra Packages for Enterprise Linux 6 - $basearch',
    enabled        => '1',
    failovermethod => 'priority',
    gpgcheck       => '1',
    gpgkey         => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6',
    mirrorlist     => 'https://mirrors.fedoraproject.org/metalink?repo=epel-6&arch=$basearch',
    require        => File['/etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6'],
  }

  # Install the Varnish Packages
  package { 'varnish':
    ensure        => '4.0.1-1.el6',
    allow_virtual => true,
    require       => [Yumrepo['epel'], Yumrepo['varnish-4.0'] ],
  }

  # Deploy the Varnish configuration
  # We'll deploy a template here, and pass in
  # values for the default backend's host
  # and port settings.  Change them below
  # and they'll be changed automatically
  # during the next puppet run

  $varnish_host = '127.0.0.1'
  $varnish_port = '8080'

  file { '/etc/varnish/default.vcl':
    ensure  => 'file',
    content => template('varnish/default.erb'),
    group   => '0',
    mode    => '0644',
    owner   => '0',
    require => Package['varnish']
  }

  # The following alternative will deploy
  # the exact contents of varnish/files/default.vcf
  # instead, but remember to comment out the
  # above section and uncomment the one below
  # to try it out
  #file { '/etc/varnish/default.vcl':
  #  ensure  => 'file',
  #  source  => 'puppet:///modules/varnish/default.vcf',
  #  group   => '0',
  #  mode    => '0644',
  #  owner   => '0',
  #  require => Package['varnish']
  #}

  # Make certain Varnish is running
  # Configured to run at system startup
  # Restart if a change in the configuration
  # deployed above is pushed out
  service { 'varnish':
    ensure    => 'running',
    enable    => true,
    require   => File['/etc/varnish/default.vcl'],
    subscribe => File['/etc/varnish/default.vcl'],
  }
}
